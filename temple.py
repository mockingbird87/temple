# Imports
import  sublime, sublime_plugin, json, os, subprocess, sys, urllib.request, fnmatch, webbrowser

from functools import partial
from os.path import expanduser


# Globals
_folder     = os.path.dirname(os.path.realpath(__file__)).replace('.sublime-package', '')
_userDir    = expanduser('~')
_grunt      = 'grunt --no-color '
_legacy     = 'grunt --gruntfile _legacy/Gruntfile.js '
_cwd        = 'cwd'
_branch     = None
_stack      = None
_version    = None
_isMac      = sys.platform == 'darwin'
_needle     = '\\' if not _isMac else '/'
_iPath      = ''
_proc       = None
_tempView   = None



###################################################################################################################################################
# Plugin Loaded
###################################################################################################################################################

def plugin_loaded():
    from Temple.process import Process
    
    global _branch
    global _stack
    global _version
    global _grunt
    global _proc
    
    _branch = settings().get('branch') 
    _stack = settings().get('stack') 
    _version = settings().get('version') 

    _proc = Process()

    if (not _branch):
        _branch = 'grunt'

    if (_stack):
        _grunt = _grunt + '--stack '
   
    if (_isMac and os.environ['PATH'].find(':/usr/local/bin/') == -1):
        os.environ['PATH'] = os.environ['PATH'] + ':/usr/local/bin/'

    sublime.active_window().status_message('Temple Extension v' + _version)

    if (settings().get('update')):
        setConfig('update', False)
        _proc.gain('update_plugin')
        sublime.message_dialog('Temple Plugin updated to version: ' + settings().get('version'))



###################################################################################################################################################
# App Commands
###################################################################################################################################################

def updatePlugin():
    s = sublime.load_settings('Temple.sublime-settings')
    s.set('update', True)
    sublime.save_settings('Temple.sublime-settings')
    executeCommand('echo Temple Plugin version: ' + settings().get('version') + ' & echo Downloading latest version...', lambda : downloadFile(), False)

class BannerOpenChrome(sublime_plugin.WindowCommand): #banner_open_chrome
    def run(self, **args):
        if (len(args) > 0):
            path = args['paths'][0].replace('\\', '/')
        else:
            path = sublime.active_window().active_view().file_name().replace('\\', '/')

        split = path.split('/')

        while ( os.path.isfile('/'.join(split) + '/index.html' ) != True and len(split) > 0 ):
            split.pop()

        if (len(split) > 0):
            path = '/'.join(split)
            path = path.split('htdocs')
            if (path[1]):
                webbrowser.open('http://localhost' + path[1])
                print('Opening in browser: http://localhost' + path[1]);
        else:
            print('No index.html found!')

        
class BannerSvnUpdate(sublime_plugin.WindowCommand): #banner_update_svn
    def run(self, **args):
        if (args['paths']):
            path = args['paths'][0].lower().replace('\\', '/')
            _proc.command('svn_update');
            _proc.trackUser('svn_update', path)
            executeCommand('svn update', None, False, path)
        

class BannerOpenCmd(sublime_plugin.WindowCommand): #banner_open_cmd
    def run(self, **args):
        if (args['paths']):
            dirname = os.path.dirname
            path = args['paths'][0]
            if (os.path.isdir(args['paths'][0]) == False):
                path = dirname(args['paths'][0])
            os.system("start cd " + path)

class BannerUpdateExtension(sublime_plugin.WindowCommand): #banner_update_extension
    def run(self, **args):
        if (sublime.ok_cancel_dialog("WARNING: You are about to update the Sublime Text Temple Plugin. Sure you want to do that?", "Yes")):
            updatePlugin()

class BannerConfig(sublime_plugin.WindowCommand): #banner_config
    def run(self, **args):
        sublime.active_window().status_message('Requesting module configurations, please wait...')
        view = sublime.active_window().active_view();
        filetype = view.file_name().split('.');
        filetype = filetype[len(filetype) - 1]

        if (filetype != 'json'):
            sublime.error_message('Can not add config object to a non config type file.')
            return

        configs = []
        u = urllib.request.urlopen('https://templeapius.herokuapp.com/get_configs')
        u = u.read().decode()
        configs = json.loads(u)
        title = []
        for c in configs:
            title.append(c['title'])

        sublime.active_window().show_quick_panel(title, lambda i: sublime.active_window().run_command('edit_config', {'i':i, 'configs': configs}) )

class EditConfigCommand(sublime_plugin.TextCommand):
    def run(self, edit, **args):
        i = args['i'];
        if (i == -1):
            return
        configs = args['configs']
        view = sublime.active_window().active_view();
        config = configs[i];
        u = urllib.request.urlopen(config['url'])
        u = u.read().decode()
        target = json.loads(u)
        body = view.substr(sublime.Region(0, view.size()))
        body = json.loads(body)

        for key in target:
            if (key not in body):
                body[key] = target[key]
            else:
                sublime.error_message('This config already includes ' + key);

        contents = json.dumps(body, indent=4, separators=(',', ': '), sort_keys=True)
        view.replace(edit, sublime.Region(0, view.size()), contents)

        _proc.gain('edit_config')
        _proc.trackUser('edit_config_' + key, _cwd)


class BannerVersion(sublime_plugin.WindowCommand): #banner_version
    def run(self, **args):
        _proc.gain('version')
        _proc.trackUser('version', getCWD())
        sublime.active_window().status_message('Temple Extension v' + _version)

class BannerSet(sublime_plugin.WindowCommand): #banner_set
    def run(self, **args):
        global _iPath
        if (args['paths']):
            path = args['paths'][0]
            _iPath = path
            if (getCWD()):
                executeCommand('set:' + path)

class BannerMigrate(sublime_plugin.WindowCommand): #banner_migrate
    def run(self, **args):
        if (getCWD()):
            cwd = _cwd.split("/")
            c1 = cwd.pop()
            c2 = cwd.pop()
            cwd = "../" + c2 + "/" + c1
            if (sublime.ok_cancel_dialog("WARNING: You are about to migrate the current project away from the old build system and on to the new one. This might have undesired effects.\nAre you sure??\n\nProject: " + cwd, "Yes")):
                if (sublime.ok_cancel_dialog("There is no going back now. Are you really really sure???\n\nProject: " + cwd, "Do it!!!")):
                    executeCommand('migrate')

class BannerCurrent(sublime_plugin.WindowCommand): #banner_current
    def run(self, **args):
        if (getCWD()):
            executeCommand('c', None)

class BannerUpdate(sublime_plugin.WindowCommand): #banner_update
    def run(self, **args):
        if (getCWD()):
            cwd = _cwd.split("/")
            c1 = cwd.pop()
            c2 = cwd.pop()
            cwd = "../" + c2 + "/" + c1
            if (sublime.ok_cancel_dialog("WARNING: You are about to update the framework for this project. This is usually not advised. Are you sure??\n\nProject: " + cwd, "Yes")):
                if (sublime.ok_cancel_dialog("Are you really really sure???\n\nProject: " + cwd, "Yes!!!")):
                    executeCommand('update_framework')

class BannerUpdateBuild(sublime_plugin.WindowCommand): #banner_update_build
    def run(self, **args):
        if (getCWD()):
            cwd = _cwd.split("/")
            c1 = cwd.pop()
            c2 = cwd.pop()
            cwd = "../" + c2 + "/" + c1
            if (sublime.ok_cancel_dialog("WARNING: You are about to update the Grunt build system. This is usually not advised. Are you sure??\n\nProject: " + cwd, "Yes")):
                if (sublime.ok_cancel_dialog("Are you really really sure???\n\nProject: " + cwd, "Yes!!!")):
                    _proc.trackUser('update_build', cwd)
                    executeCommand('git fetch origin', 
                        lambda : executeCommand('git reset --hard origin/' + _branch, 
                            lambda : executeCommand('npm install', 
                                lambda : _proc.gain('update_build'), False), False),
                    False)

class BannerBuildVariationsCurrent(sublime_plugin.WindowCommand): #banner_build_variations_current
    def run(self, **args):
        cwd = getCWD()
        if (cwd):
            variationsQuickPick(cwd, lambda args: executeCommand('build:' + args))

class BannerBuildCurrent(sublime_plugin.WindowCommand): #banner_build_current
    def run(self, **args):
        if (getCWD()):
            current = getCurrentProject()[0]
            _proc.trackUser('build_current', _cwd)
            executeCommand('build:' + current.replace(' > ', ':'))

class BannerBackupCurrent(sublime_plugin.WindowCommand): #banner_backup_current
    def run(self, **args):
        cwd = getCWD()
        if (cwd):
            current = getCurrentProject()[0]
            hasbackup = findBackupFunction(cwd, current)
            if(hasbackup == False):
                if (sublime.ok_cancel_dialog("Missing onBackupImage function.\nPlease add banner.prototype.onBackupImage to your project.\nDo it afterall?", "Yes I dont care")):
                    self.doit(current)
            else:
                self.doit(current)                    

    def doit(self, current, **args):
        _proc.trackUser('backup_current', _cwd)
        host = settings().get('localhost')
        if (host):
            host = ' --localhost=' + host.lower()
        else:
            host = ''
        executeCommand('backup:' + current.replace(' > ', ':') + host)

class BannerBuild(sublime_plugin.WindowCommand): #banner_build
    def run(self, **args):
        if (getCWD()):
            projectQuickPick(lambda args: executeCommand('build:' + args))


class BannerBuildVariations(sublime_plugin.WindowCommand): #banner_build_variations
    def run(self, **args):
        cwd = getCWD()
        if (cwd):
            variationsQuickPick(cwd, lambda args: executeCommand('build:' + args), True)

class BannerLegacyBuild(sublime_plugin.WindowCommand): #banner_build
    def run(self, **args):
        if (getCWD()):
            _proc.trackUser('legacy_build', _cwd)
            projectQuickPick(lambda args: executeCommand(_legacy + 'build:' + args, None, False))

class BannerBackup(sublime_plugin.WindowCommand): #banner_backup
    def run(self, **args):
        if (getCWD()):
            host = settings().get('localhost')
            if (host):
                host = ' --localhost=' + host.lower()
            else:
                host = ''
            projectQuickPick(lambda args: executeCommand('backup:' + args + host))

class BannerTinypng(sublime_plugin.WindowCommand): #banner_tinypng
    def run(self, **args):
        if (getCWD()):
            projectQuickPick(lambda args: executeCommand('tiny_png:' + args))

class BannerInit(sublime_plugin.WindowCommand): #banner_init
    def run(self, **args):
        if (getCWD()):
            projectQuickPick(lambda args: checkInitVariations(args))
            # projectQuickPick(lambda args: executeCommand('init:' + args))

class BannerCustom(sublime_plugin.WindowCommand): #banner_custom
    def run(self, **args):
        if (getCWD()):
            sublime.active_window().show_input_panel('Enter custom task name', '', self.onSelect, None, hidePopup)
            sublime.set_timeout(lambda : showPopUp('Enter custom task name'), 100)

    def onSelect(self, file=None, **args):
        if (os.path.isfile(_cwd + '/_grunt/tasks/' + file + '.js')):
            sublime.error_message('Warning: Custom tasks file \''+ file + '.js\' already exists!')
            sublime.active_window().open_file(_cwd + '/_grunt/tasks/' + file + '.js')
        else:
            if (file != ''):
                executeCommand('create_task:' + file, lambda : sublime.active_window().open_file(_cwd + '/_grunt/tasks/' + file + '.js')) 
        hidePopup()


class BannerCreate(sublime_plugin.WindowCommand): #banner_create
    def run(self, **args):
        global _iPath
        _iPath = False

        if (args):
            if (args['paths']):
                _iPath = args['paths'][0]

        if (getCWD()):
            if (_iPath):
                if (os.path.isdir(_iPath) == False):
                    _iPath = os.path.abspath(os.path.join(_iPath, os.pardir))

                if (os.path.exists(_iPath + _needle  + '_grunt') == False):
                    os.makedirs(_iPath + _needle  + '_grunt')
                
                cwd = _iPath.lower()                
            else:
                cwd = _cwd

            sublime.active_window().show_input_panel('Enter a config filename', '', lambda file: self.onSelect(file, cwd), None, hidePopup)
            
            sublime.set_timeout(lambda : showPopUp('Enter a config filename'), 100)

    def onSelect(self, file=None, cwd=None, **args):
        if (os.path.isfile(cwd + '/_grunt/' + file + '.json') and not sublime.active_window().active_view().hide_popup()):
            sublime.error_message('Warning: Config file already exists.')
            sublime.active_window().open_file(cwd + '/_grunt/' + file + '.json')
        else:
            createNewConfig(file, cwd + '/_grunt/' + file + '.json')

        hidePopup()

###################################################################################################################################################
# EventListener
###################################################################################################################################################

class EventListener(sublime_plugin.EventListener):
    def on_close(self, view):
        if not sublime.windows():
            cleanup()

    def on_activated(self, view):
        if (view.file_name()):
            fileCWD = getFileCWD(view.file_name())

            if(fileCWD):
                getCWD()

                if(fileCWD != _cwd):
                    setCWD(fileCWD)

    def on_post_save(self, view):
        filetype = view.file_name()[view.file_name().rfind('.'):] 

        if (filetype == '.scss'):
            build = getGruntRoot()
            _proc.command('compile_css')
            _proc.trackUser('compile_css', _cwd)
            if (_isMac):
                sublime.set_timeout_async(lambda : subprocess.getoutput("cd "+build + " && grunt compile_css"), 100)
            else:
                sublime.set_timeout_async(lambda : subprocess.getoutput("cmd /c \"cd "+build + " & grunt compile_css"), 100)
            output = sublime.active_window().get_output_panel('exec') 
            onCommandComplete(output, 0, 'Finished', commandCallback)

        if (filetype == '.css'):
            executeCommand('compile_css');

        if (filetype == '.es6'):
            executeCommand('compile_es6');


###################################################################################################################################################
# Logic
###################################################################################################################################################


def checkInitVariations(args):
    proj = open(_cwd + '/_grunt/' + args + '.json', 'r').read()
    proj = json.loads(proj)
    formats = []
    for f in proj['formats']:
        if (len(f['languages']) > 1 or len(f['variations']) > 1):
            formats.append(f)

    if (len(formats)):
        if(sublime.ok_cancel_dialog("It looks like you have multiple languages / variations in your project. Would you like to create individual config files for these languages / variations? ", "Yes")):
            executeCommand('init:' + args + ':true')
        else:
            executeCommand('init:' + args)


def findBackupFunction(cwd, current):
    hasBackup = False
    current = current.split(' > ')[0]
    proj = cwd + "/" + current
    for root, dirnames, filenames in os.walk(proj):
        for filename in fnmatch.filter(filenames, '*.js'):
            with open(os.path.join(root, filename)) as main:
                if(main.read().find('.onBackupImage') > -1):
                    hasBackup = True
    
    return hasBackup

def restarSublime():
    if (_isMac):
        subprocess.call("pkill subl && "+ os.path.join(os.getcwd(), 'subl'), shell=True)
    else:
        subprocess.call('taskkill /im sublime_text.exe /f && cmd /C "'+ os.path.join(os.getcwd(), 'sublime_text.exe') + '"', shell=True)

def downloadFile():
    urllib.request.urlretrieve ('https://docs.google.com/uc?export=download&id=0B49FFVOwtJPKSHZoVGJ1bmJiZlU', _folder)
    sublime.active_window().run_command('hide_panel')
    sublime.message_dialog('Temple Plugin downloaded successfully.\nPlease restart Sublime Text to finish install.')
    restarSublime()

def find_between( s, first, last ):
    s = str(s, 'utf-8')

    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ''

def hidePopup():
    global _tempView

    sublime.active_window().active_view().hide_popup()
    if (_tempView):
        _tempView.set_scratch(True)
        _tempView.close()
        _tempView = None

def showPopUp(label="Type below"):
    global _tempView

    view = sublime.active_window().active_view()
    if (view):
        try:
            popup = sublime.load_resource('Packages/Temple/popup.html')
        except:
            popup = sublime.load_resource('Installed Packages/Temple/popup.html')

        scheme_path = view.settings().get('color_scheme')
        theme_name = sublime.load_settings('Preferences.sublime-settings').get('theme')
        scheme_content = sublime.load_binary_resource(scheme_path)
        style = find_between(scheme_content, '<key>background</key>', '</string>')
        style = style.replace('<string>', '')
        style = style.replace(' ', '')
        style = style.replace('\n', '')
        style = style.replace('\t', '')
        content = popup.replace('\n', '')
        content = content.replace('{COLOR}', style)   

        size = sublime.active_window().active_view().viewport_extent()
        rows = int(size[1] / 15)
        n = ''
        i = 0

        while(i < rows):
            i += 1
            n += '\n'

        _tempView = sublime.active_window().new_file()
        _tempView.set_name(label)
        _tempView.run_command("insert", {"characters": n})

        region = _tempView.visible_region()
        lines = _tempView.lines(region)
        i = len(lines) - 7
        try:
            _tempView.show_popup(content, location=lines[i].begin())
        except:
            print('Can\'t show popup.')
        

def cleanup(callback=False):
    if (_isMac):
        cmd = 'echo node'
    else :
        cmd = 'taskkill /f /im node.exe'
    executeCommand(cmd, callback, False)

def executeCommand(cmd, callback=False, grunt=True, cwd=False):
    sp = cmd.split(':')
    if(len(sp) == 3 and _proc):
        _proc.trackUser(None, None, sp[2])

    if (grunt): 
        if (_proc):
            _proc.command(cmd.split(':')[0])
            _proc.trackUser(cmd.split(':')[0], getCWD())

        cmd = _grunt + cmd

    if (cmd.find('legacy', 0) > 1 and _proc):
        _proc.command(cmd.split(':')[0].replace(_legacy, ''))

    args = {
        'cmd': cmd, 
        'shell': True, 
        'working_dir': cwd or getGruntRoot()
    }

    if (_isMac and (cmd.find('legacy', 0) > 1 or grunt)):
        args['path'] = '/usr/local/bin/:$PATH'
   
    sublime.active_window().run_command('exec', args)
    # print('\n'*200)
    output = sublime.active_window().get_output_panel('exec') 
    onCommandComplete(output, 0, 'Finished', commandCallback, callback)

def commandCallback(output, callback=None):
    if (output.find('>> Error task failed!', 0).begin() > -1 or
        output.find('Aborted due to warnings.', 0).begin() > -1 or
        output.find('Error: ', 0).begin() > -1 or
        output.find('with exit code.', 0).begin() > -1):
        _proc.lose()
    else:
        _proc.gain()
    if (callback): callback()

def onCommandComplete(output, i, needle, callback=False, exCallback=False):
    if (output.find(needle, 0).begin() > -1):
        callback(output, exCallback)
    else:
        i += 1
        if (i < 1000):
            sublime.set_timeout(partial(lambda : onCommandComplete(output, i, needle, callback, exCallback)), 200)

def projectQuickPick(callback=None, listAll=False):
    projects = getCurrentProject() + getBuildableProjects()

    sublime.active_window().show_quick_panel(projects, lambda i:
            callback(projects[i].replace(' > ', ':')) if i > -1 and callback != None else None
        )

def variationsQuickPick(cwd=None, callback=None, listAll=False):
    projects = getBuildableProjects()
    variations = []

    if (listAll):
        for proj in projects:
            projPath = cwd + "/" + proj
            for root, dirnames, filenames in os.walk(projPath):
                for filename in fnmatch.filter(filenames, 'config_*'):
                    var = filename.replace('.json', '').split('_')[1]
                    variations.append(proj + " > " + var)
        variations = list(set(variations))
        sublime.active_window().show_quick_panel(variations, lambda i:
            callback(variations[i].replace(' > ', ':undefined:')) if i > -1 and callback != None else None
        )
        return

    current = getCurrentProject()[0]
    current = current.split(' > ')
    proj = cwd + "/" + current[0] + "/" + current[1]
    
    for root, dirnames, filenames in os.walk(proj):
        for filename in fnmatch.filter(filenames, 'config_*'):
            var = filename.replace('.json', '').split('_')[1]
            variations.append(" > ".join(current) + " > " + var)

    sublime.active_window().show_quick_panel(variations, lambda i:
            callback(variations[i].replace(' > ', ':').replace(' > ', ':')) if i > -1 and callback != None else None
        )

def createNewConfig(title, path):
    sublime.active_window().active_view().hide_popup()
    obj = {
        "project_title": title,
        "cloud_hosting": False,
        "formats": [{
            "title": "master_300x250",
            "size": {
                "width": 300,
                "height": 250
            },
            "platforms": ["doubleclick"],
            "modules": [],
            "variations": [],
            "languages": [],
            "build": {
                "flatten": [],
                "minify": [],
                "exclude": []
            }
        }],
        "build": {
            "modules": [],
            "flatten": [],
            "minify": [],
            "exclude": [],
            "inline": [],
            "backup": [],
            "tinypng": False,
            "tinyjpg": False,
            "custom_tasks": {}
        }
    };

    file = open(path, 'w')
    file.write(json.dumps(obj, indent=4, separators=(',', ': ')))
    file.close()
    sublime.active_window().open_file(path)

    sublime.set_timeout(lambda : _proc.gain('create'), 100)



###################################################################################################################################################
# Getters
###################################################################################################################################################

def getCurrentProject():
    projects = []
    file = sublime.active_window().active_view().file_name()

    if (not file):
        return []

    file = file.split(_needle)
    
    if ('_grunt' not in file):
        p = getBuildableProjects()

        for i in p:
            with open(_cwd + '/_grunt/' + i + '.json') as data:  
                j = json.load(data)

                for f in j['formats']:
                    try:
                        id = file.index(j['project_title'])
                        title = file[id + 1]

                        if (f['title'] == title):
                            first = i + ' > ' + title
                            projects.append(first)
                    except:
                        sublime.active_window().status_message('Warning! Current file not in Current Working Directory.')
        return projects
    return []

def getBuildableProjects():
    try:
        files = os.listdir(_cwd + '/_grunt')
    except:
        sublime.error_message('The current working directory is not a grunt project. Please create a grunt folder or set a different current working directory.\n\nCWD : ' + _cwd)
        return
    projects = []
    for f in files:
        if ('.json' in f):
            projects.append(f.replace('.json', ''))
    return projects

def returnCWD():
    return _cwd

def getCWD():
    if (not getGruntRoot()):
        args = {
            'cmd': 'echo \"Temple plugin error! Please set your build root path in the Temple user config. Preferences > Package Settings > Temple > Settings - User\"', 
            'shell': True, 
            'working_dir': getGruntRoot(),
        }
        sublime.active_window().run_command('exec', args)
        return
    
    try:
        with open(_userDir + '/.banner-config', 'r') as data:  
            global _cwd
            _cwd = json.load(data)['cwd']
        return _cwd
    except:
        sublime.status_message('Error loading: ' + _userDir + '/.banner-config. I think the file is broken. ¯\_(ツ)_/¯')
        print('Error loading .banner-config. I think the file is broken. Please check if the json is valid.')
        fileCWD = ""
        try:
            fileCWD = getFileCWD(sublime.active_window().active_view.file_name())
        except:
            print('no file cwd')
            fileCWD = getGruntRoot()
        print('fileCWD')
        print(fileCWD)

        data = {"cwd":fileCWD}

        with open(_userDir + '/.banner-config', 'w') as outfile:
            json.dump(data, outfile)
            print(data)
            #setCWD(fileCWD)

        return False

def getFileCWD(file):
    cwd = False
    split = file.split(_needle)
    split.pop()

    for i in split:
        path = '/'.join(split)

        if (os.path.isdir(path + '/_grunt')):
            cwd = path.lower()
            break

        split.pop()

    return cwd

def getGruntRoot(p=None):
    global _iPath
    path = settings().get('root')
    isBuild = False    

    if (not path):
        sublime.status_message("ERROR: No root settings found. Creating!")
        fileCWD = sublime.active_window().active_view().file_name()

        if (_iPath):
            fileCWD = _iPath

        if (fileCWD):
            split = ''
            split = fileCWD.split(_needle)

            for i in split:
                path = path + i + '/'
                if (i == 'banners'):
                    isBuild = True
                    break

            path = path + '_build'

        if (len(path) < 10 or isBuild == False):
            sublime.error_message('Temple plugin error!\nPlease set your build root path in the Temple user config.\nPreferences > Package Settings > Temple > Settings - User')
            return None

        setConfig('root', path)
    return path

def settings():
    return sublime.load_settings('Temple.sublime-settings')
    


###################################################################################################################################################
# Setters
###################################################################################################################################################   

def setCWD(cwd):
    executeCommand('set:' + cwd)

def setConfig(key, data):
    s = sublime.load_settings('Temple.sublime-settings')
    s.set(key, data)
    sublime.save_settings('Temple.sublime-settings')