# Imports
import sublime, sublime_plugin, json, urllib.request, sys, os, datetime, re, Temple.temple, random, string, threading, subprocess
from urllib.parse import urlencode
from os.path import expanduser
from time import sleep
from subprocess import Popen, PIPE, STDOUT


# Globals
_st3 		= int(sublime.version()) >= 3000
_folder 	= os.path.dirname(os.path.realpath(__file__)).replace('.sublime-package', '')
_isMac 		= sys.platform == 'darwin'
_needle 	= '\\' if not _isMac else '/'
_enc 		= _folder.split('\\').pop().replace('e','3') if not _isMac else _folder.split('/').pop().replace('e','3')
_api        = 'https://templeapius.herokuapp.com/'
# _api        = 'http://localhost:5000/'
_resourceP  = 'Packages/'
_lib        = ''
_regions    = {
    'US': 'America/Los_Angeles',
    'NL': 'Europe/Amsterdam',
    'AR': 'America/Argentina/Buenos_Aires'
}
_popupData	= []





class BannerSendGift(sublime_plugin.WindowCommand): #banner_update_extension
    def run(self, **args):
        try:
            u = urllib.request.urlopen(_api + 'get_users')
            u = json.loads(u.read().decode())
            
            if(len(u) == 1):
                sublime.message_dialog("I'm sorry but this feature is not quite ready yet.\nBut don't worry, it will be ready soon :)")                
                return

            names = []
            for user in u:
                names.append(user["name"])
            sublime.active_window().show_quick_panel(names, lambda i: self.input(u[i]) if i > -1 else None )
        except ValueError as e:
            print('can not load users')

    def input(self, user, **args):
        if(Temple.temple._proc.user._userdata["name"] == user["name"]):
            sublime.message_dialog("You can not send a gift to yourself! Shame on you :(")
            return
        sublime.active_window().show_input_panel('How many gifts will you send to '+user["name"]+'? (max 100)', '20', lambda a: self.send(a, user), None, None)

    def send(self, amount, user, **args):
        _user = Temple.temple._proc.user
        _data = Temple.temple._proc._data

        if (int(amount) > 100):
            sublime.message_dialog("I'm sorry but you can't send that many gifts today")            
            return

        now = datetime.datetime.now().timestamp()
        u = _user.cooldown("send_gift")
        d = int(now) - int(u)
        cooldown = int(_data['commandScores']["send_gift"]['cooldown']) * 60
        dd = datetime.timedelta(seconds = cooldown-d)
        dd = str(dd).split(':')
        ddout = ""

        if (int(dd[0]) > 0):
            ddout = ddout + dd[0] + " hours"

        if (int(dd[1]) > 0):
            ddout = ddout + ", " + dd[1] + " minutes"
            
        if (int(dd[2]) > 0):
            ddout = ddout + " and " + dd[2] + " seconds"


        if (int(d) < int(cooldown)):
            sublime.message_dialog("I'm sorry but you can't send more gifts today.\n\nTry again in " + ddout)
            return


        data = {
            "sender": _user._userdata["name"],
            "sender_id": _user._userdata["id"],
            "sendee": user["name"],
            "sendee_id": user["id"],
            "amount": amount
        }
        
        s = json.dumps(data)
        s = encrypt(s)

        data = {
            "data": s
        }

        data = json.dumps(data).encode('UTF-8')

        url = urllib.request.Request(_api + "sg", data=data, headers={'content-type': 'application/json'})

        r = urllib.request.urlopen(url).read().decode('utf8', 'ignore')
        r = json.loads(r)

        if(r["success"] == True):
            sublime.message_dialog("Awesome!! " + user["name"] + " will be so happy :D!!\nPlease have some extra EXP for being such a great colleague!!!")
            Temple.temple._proc.gain("send_gift")
        else:
            sublime.message_dialog("Whoops.. somethings not right there.\n\n\""+r["error"]+"\"")
            if(r["lose"]):
                Temple.temple._proc.lose("send_gift")

        



class Process(object):

    def __init__(self):
        global _lib 

        self._ready = False
        self._command = 'False'

        timeout = 0

        _lib = _folder.replace('\Temple', '\lib')

        if (os.path.isdir(_lib) == False):
            os.mkdir(_lib)
            try:
                lib_openssl = sublime.load_binary_resource('Packages/Temple/openssl.exe')
                lib_aey32 = sublime.load_binary_resource('Packages/Temple/libeay32.dll')
                lib_sslaey = sublime.load_binary_resource('Packages/Temple/ssleay32.dll')
            except:
                lib_openssl = sublime.load_binary_resource('Installed Packages/Temple/openssl.exe')
                lib_aey32 = sublime.load_binary_resource('Installed Packages/Temple/libeay32.dll')
                lib_sslaey = sublime.load_binary_resource('Installed Packages/Temple/ssleay32.dll')
                
            lib_openssl_file = open(_lib + '\openssl.exe', 'wb')
            lib_openssl_file.write(lib_openssl)
            
            lib_aey32_file = open(_lib + '\libeay32.dll', 'wb')
            lib_aey32_file.write(lib_aey32)

            lib_sslaey_file = open(_lib + '\ssleay32.dll', 'wb')
            lib_sslaey_file.write(lib_sslaey)

            timeout = 1000

        
        sublime.set_timeout_async(self.loadData, timeout)

        # set_interval(self.getData, 1800 + timeout)


    def loadData(self):
        self.getData()

        self.createUser()

        self._ready = True

        print('Game ready')

    def getData(self):
        try:
            u = urllib.request.urlopen('https://s3-us-west-1.amazonaws.com/templedata/data')
            u = u.read().decode()
        except:
            u = sublime.load_resource(_resourceP + 'Temple/data')

        try:
            uj = json.loads(u)
            ue = encrypt(u)
            file = open(_folder + '/data', 'w')
            file.write(ue)
            file.close()
        except:
            ue = decrypt(u)
            u = str(ue, 'latin-1')

        self._data = json.loads(u)

        
    def createUser(self):
        self.user = User(self._data)

    def echo(self, string):
        if (not self._ready): return

        print('Game says: ' + string)
        sublime.active_window().status_message(string)

    def getScore(self, type):
        if (not self._ready): return 0

        if (self._command != 'False'):
            return str(self._data['commandScores'][self._command][type])
        else: 
            return 0

    def showPopUp(self, data, type='bronze'):
        if (not self._ready): return

        if (hasattr(data, 'cup')):
            type = data['cup']

        view = sublime.active_window().active_view()
        if (view):
            if (view.is_popup_visible()):
                _popupData.append(data)
            else:
                _popupData = [data]

            content = '<style>body{background-color:#1c1c1c} img {clear:both;}</style><div><img src="' + self._data[type] + '"></div>'
            k = 0
            for img in _popupData:
                k += 1
                content += ('<div><img src="' + img['badge'] + '"></div>')

            region = view.visible_region()
            lines = view.lines(region)
            view.show_popup(content, location=lines[0].begin(), max_width=200, max_height=500)


    def trackUser(self, stat=None, cwd=None, format=None):
        if (not self._ready): return

        self.user.track(stat, cwd, format)


    def command(self, c):
        if (not self._ready): return

        if (not c in self._data['commandScores']):
            self._command = 'False'
            return

        if (self._data['commandScores'][c]):
            self._command = c
        else:
            self._command = 'False'

    def lose(self, c=None):
        if (not self._ready): return

        if (c): self.command(c)
        if (self._command != 'False'):
            exp = self._data
            self.echo('Exp. lost -' + self.getScore('lose'))
            self.user.lose(self.getScore('lose'))
            self._command = 'False'

    def gain(self, c=None):
        if (not self._ready): return

        if (c): self.command(c)
        
        if (self._command != 'False'):
            now = datetime.datetime.now().timestamp()
            u = self.user.cooldown(self._command)
            d = int(now) - int(u)
            cooldown = int(self._data['commandScores'][self._command]['cooldown']) * 60
            dd = datetime.timedelta(seconds = cooldown-d)

            if (int(d) < int(cooldown)):
                self.echo('Cooldown ' + self._command + ' -' + str(dd))
                self._command = 'False'
                return

            self.user.cooldown(self._command, True)
            exp = int(self.getScore('score')) * int(self._data['multiplier'])
            self.echo('Exp. gained +' + str(exp))
            self.user.gain(self.getScore('score'))
            self._command = 'False'


class User(object):

    def __init__(self, data):
        self._ready = False
        self._data = data
        self._path = sublime.packages_path() + _needle + '..' + _needle + 'Local' + _needle + 'Database.sublime_database'
        if os.path.isfile(self._path):
            file = open(self._path, 'r')
            d = decrypt(file.read())
            self._userdata = json.loads(binaryToString(d))
            self.setDataApi(self._userdata['country'])
            self._ready = True
            sublime.set_timeout_async(lambda : self.getBiertijdMultiplier())
        else:
            sublime.set_timeout(lambda : Temple.temple.showPopUp('Please tell me your name'), 100)
            sublime.active_window().show_input_panel('Please tell me your name', '', self.createUser, None, self.createUser)

    def getBiertijdMultiplier(self):
        try:
            biertijd = urllib.request.urlopen('https://ishetalbiertijd.nl/api/mediamonks/status?timezone=' + _regions[self._userdata['country']])
            biertijd = json.loads(biertijd.read().decode())['data']
        except:
            biertijd = { "status": False }

        if (biertijd['status']):
            self._data['multiplier'] = int(self._data['multiplier']) * int(self._data['biertijd_multiplier'])
            sublime.set_timeout(lambda : Process.echo(self,'It\'s beertime!! Get ' + str(self._data['multiplier']) + 'x multiplier on all exp!'), 1200)


    def createNewUser(self, username=False):
        sublime.set_timeout(lambda : sublime.error_message('Your user data has been compromised! Your gonna have to start all over again...'), 1000)
        os.remove(self._path)
        self.compromised = True
        self.__init__(self._data)


    def createUser(self, username=False):
        Temple.temple.hidePopup()
        if (username): username = username.strip().replace(' ', '_')
        username = username if username else os.getlogin()

        try:
            country = urllib.request.urlopen('http://freegeoip.net/json/')
            country = json.loads(country.read().decode())
        except:
            country = { "country_code": "NL" }

        self._userdata = {
            "name": username,
            "country": country['country_code'],
            "exp": 0,
            "collected": 0,
            "rank":"",
            "cooldown": {},
            "achievements": [],
            "stats": {
                "tasks": {},
                "projects": {},
                "formats": {}
            }
        }

        self.setDataApi(country['country_code'])

        cname = self._userdata['name'][0].upper() + self._userdata['name'][1:]
        sublime.set_timeout(lambda : Process.echo(self, 'Game initiated: Good luck ' + cname + '!'), 500)

        sublime.set_timeout_async(lambda : self.getBiertijdMultiplier())
        self.save(True)
        self._ready = True

        if (hasattr(self, 'compromised')):
            if (self.compromised == True):
                self.compromised = False
                self.gain(self.data['reset'])
                sublime.set_timeout(lambda : Process.echo(self, 'Exp. gained +' + str(self.data['reset'])), 1000)
        # self.collectData()

    def setDataApi(self, country):
        global _api

        if (country == 'NL'):
            _api = 'https://templeapieu.herokuapp.com/'
        else:
            _api = 'https://templeapius.herokuapp.com/'

    def sendData(self, create=False):
        data = json.dumps(self._userdata).encode('UTF-8')
        if (create):
            url = urllib.request.Request(_api + self._userdata['name'], data=data, headers={'content-type': 'application/json'})
        else:
            url = urllib.request.Request(_api + 'update/' + self._userdata['name'], data=data, headers={'content-type': 'application/json'})

        r = urllib.request.urlopen(url).read().decode('utf8', 'ignore')
        r = json.loads(r)
        
        if (r["success"] == False or r['id'] == False):
            self.createNewUser()
            return

        if ('id' in r):
            self._userdata['id'] = r['id']

    def collectData(self, save=True, create=False):
        if (not hasattr(self, '_userdata')):
            return

        now = datetime.datetime.now().timestamp()
        u = self._userdata['collected']
        d = int(now) - int(u)

        self._userdata['collected'] = now
        sublime.set_timeout_async(lambda : self.sendData(create), 0)
			
    def track(self, stat=None, cwd=None, format=None):
        if (format):
            size = False
            regex = r"((?:(?:[\d\.]+)(?:\s*x\s*(?:[\d\.]+))+)(?:\s*(?:px|mm))?)"
            matches = re.finditer(regex, format)
            for num, match in enumerate(matches):
                size = match.group()
                break;
			
            if (size):
                if (not size in self._userdata['stats']['formats']):
                    self._userdata['stats']['formats'][size] = 1
                else:
                    self._userdata['stats']['formats'][size] = self._userdata['stats']['formats'][size] + 1
            return

        split = cwd.split('/')
        a = split.pop()
        b = split.pop()
        project = b + '-' + a

        if (not stat in self._userdata['stats']['tasks']):
            self._userdata['stats']['tasks'][stat] = 1
        else:
            self._userdata['stats']['tasks'][stat] = int(self._userdata['stats']['tasks'][stat]) + 1

        if (not project in self._userdata['stats']['projects']):
            self._userdata['stats']['projects'][project] = {'tasks':{},'formats':{}}

        if (not stat in self._userdata['stats']['projects'][project]['tasks']):
            self._userdata['stats']['projects'][project]['tasks'][stat] = 1
        else:
            self._userdata['stats']['projects'][project]['tasks'][stat] = int(self._userdata['stats']['projects'][project]['tasks'][stat]) + 1


    def save(self, create=False):
        s = json.dumps(self._userdata)
        s = stringToBinay(s)
        s = encrypt(s)
        file = open(self._path, 'w')
        file.write(s)
        file.close()
        self.collectData(False, create)

    def cooldown(self, c, clear=False):
        if (clear):
            self._userdata['cooldown'][c] = datetime.datetime.now().timestamp()
            return self._userdata['cooldown'][c]

        if (not c in self._userdata['cooldown']):
            self._userdata['cooldown'][c] = 0
		
        return self._userdata['cooldown'][c]

    def gain(self, exp):
        self._userdata['exp'] = int(self._userdata['exp']) + (int(exp) * int(self._data['multiplier']))
        self.checkAchievements()
        sublime.set_timeout_async(self.save, 0)

    def lose(self, exp):
        self._userdata['exp'] = int(self._userdata['exp']) - int(exp)
        if (self._userdata['exp'] < 0): self._userdata['exp'] = 0
        sublime.set_timeout_async(self.save, 0)

    def checkAchievements(self):
        exp = self._userdata['exp']
        rank = self._userdata['rank']
        userachv = self._userdata['achievements']
        ranks = self._data['ranks']
        achv = self._data['achievements']
        toGain = False
        for i in ranks:
            nextOne = False
            for ur in userachv:
                if (ur['title'] == i['title']):
                    nextOne = True
            if (nextOne): continue
            if (exp >= int(i['exp']) and rank != i['title']):
                toGain = i

        if (toGain): self.gainRank(toGain)

        gainAchievement = []

        for a in achv:
            nextOne = False
            for ua in self._userdata['achievements']:
                if ('id' in ua):
                    if ua['id'] == a['id']:
                        nextOne = True
                        break
            
            if (nextOne): continue

            conditions = a['conditions']
            
            for c in conditions:
                if (c == 'id'):
                    if ('id' in self._userdata):
                        if (conditions[c] == self._userdata['id']):
                            break

                if c in self._userdata['stats']['tasks']:
                    if (self._userdata['stats']['tasks'][c] < conditions[c]):
                        nextOne = True
                        break
                else:
                    nextOne = True
                    break

            if (nextOne): continue

            gainAchievement.append(a)

        if (len(gainAchievement) > 0):
            self._userdata['achievements'].extend(gainAchievement)
            for a in gainAchievement:
                self._userdata['exp'] = int(self._userdata['exp']) + (int(a['reward']))
                Process.echo(self, 'Achievement Unlocked!! - ' + a['title'] + "   Exp. gained +" + str((int(a['reward']))))
                sublime.set_timeout(lambda : Process.showPopUp(self, a), 3000)
            sublime.set_timeout_async(self.save, 0)

    def gainRank(self, rank):
        self._userdata['achievements'].append({
            'type': 'Gained Rank',
            'title': rank['title'],
            'rank': rank
            })
        self._userdata['exp'] = int(self._userdata['exp']) + (int(rank['reward']))
        self._userdata['rank'] = rank['title']
        sublime.set_timeout_async(self.save, 0)
        Process.echo(self, 'Gained Rank!! - ' + rank['title'] + "   Exp. gained +" + str((int(rank['reward']))))
        sublime.set_timeout(lambda : Process.showPopUp(self, rank), 1000)

    def getData(self, **args):
        return self._userdata



def crypto(enc_flag, enc, data):
    cipher = '-aes128'

    if (_isMac):
        openssl_command = os.path.normpath( 'openssl' )
    else:
        openssl_command = os.path.normpath( _lib + '\openssl' )

    envVar = ''.join( random.sample( string.ascii_uppercase, 23 ) )
    os.environ[ envVar ] = enc
    
    os.environ[ 'OPENSSL_CONF' ] = _lib + '\openssl.cnf' 
    env = "env:%s" % envVar
    
    try:
        home = expanduser("~")
        _data = open(home+'\\tpl_data', 'w')
        _data.write(data)
        _data.close()
        # dec = "U2FsdGVkX1/64BBHthqk53Wb3KIiJw7IcfLHlJbDTsaT5v81fX1kmixZEYQr4ZW1"

        result = subprocess.check_output(["openssl", "enc", enc_flag, cipher, "-base64", "-pass", env, "-in", home+'\\tpl_data'], shell=True)
        # result = subprocess.check_output(["echo", data, "|","openssl", "enc", enc_flag, cipher, "-base64", "-pass", env], shell=True)
        # print(output)
        os.remove(home+'\\tpl_data')
        # openssl = Popen(["openssl", "enc", enc_flag, cipher, "-base64", "-pass", env], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        # result, error = openssl.communicate( data.encode("utf-8") )
    except IOError as e:
        error_message = "Crypto Error: %s" % e
        print(error_message)
        return False
    except OSError as e:
        error_message = """
        Please verify that you have installed OpenSSL.
        Attempting to execute: %s
        Error: %s
        """ % (openssl_command, e[1])
        print(error_message)
        return False

    return result

def encrypt(data):
	return str(crypto('-e', _enc, data), 'utf-8')

def decrypt(data):
	return crypto('-d', _enc, data)

def stringToBinay(string):
    return ''.join('{:08b}'.format(ord(c)) for c in string)

def binaryToString(binary):
    return ''.join(chr(int(binary[i:i+8], 2)) for i in range(0, len(binary), 8))


def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t