# README #

MediaMonks Temple plugin

## Tasks ##

### Build Current ###
Run the grunt build command on the current creative in view

### Build Current Variations ###
Run the grunt build command on the current creative in view and only build specific variations

### Backup Current ###
Run tha grunt backup command on the current creative in view

### Build ###
Build all creatives in a specified project

### Build Variations ###
Build all creatives in a specified project and only of a specific variation

### Create ###
Create a new project config file

### Init ###
Initiate a new project based on a specified config file

### Backup ###
Create backup images for all creatives in a project

### Custom Task ###
Create a new custom grunt task file

### TinyPNG ###
Run the grunt tinypng task on all png images in the project

### Migrate ###
Migrate old framework to new

### Legacy Build ###
Build older projects

### Append Config ###
Append modules to the creative config file

### Send Gift ###
Send a colleague EXP points



## Changelog - Aug 7 2017 ##

### Open in Chrome ###
Right click any file / folder in the sidebar and open that baby up in Chrome, or your favorite browser. [Pro-throwback-tip] Hit Ctrl + Enter 😉

### Open Command window ###
Also a new command added to the Sidebar Context menu. Hit that to open a command window within the selected folder. That is.. if your manly (/ womanly)** enough...

### SVN update ### 
SVN update a project directly from the Sidebar. Git commands coming soon!

### More Ninja ### 
In the words of our very own Delilah, the Sublime Plugin now works more "Ninja"-like, meaning, no more annoying command window popups when you run commands. Whoohoo!!

### Build variations ### 
If you have a project that has config variations eg. config_nl or config_en, you can now specify which variation to build. If you are so inclined.

### Init with config variations ### 
Ever wanted to init a project and have it create a separate config.json for each language or variation? Now you can :). Just run the init command "exactly as you always do" and if you have languages or variations defined, the plugin will ask you if you want to create configs for these. Wow!

### Send gift ### 
The most exciting update of them all!!! You asked for it and now it's here... okay almost here. I still need to finish up some security issues before it's completely released. Wouldn't want you guys hacking the system now 😏 But expect a PSA on this soon enough.

